**Task 15 - Guessing Game**

- Create a Guessing Game in JavaScript 
- Guess a number between 0 and 100
- If too high, display a message on screen : 
    - You are too high,
    - Low: You are too low,
    - Right: YOU ARE RIGHT, 
- Show restart button: reset the game.
