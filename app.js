const randNumber = Math.floor(Math.random() * 101); 
let guesses = 1;

document.getElementById("submitGuess").onclick = function (){
    let guess = parseInt(document.getElementById("guessField").value);

    if (isNaN(guess) || guess < 0 || guess > 100 || guess === "") {
        text = "Input not valid";
        document.getElementById("inputMessage").innerHTML = text;
    } else if (randNumber == guess) { // Initial condition
        alert('YOU ARE RIGHT!!' + ' ' + 'Total:' + ' ' + guesses + ' ' + 'guesses');
    } else if (randNumber > guess) { //Second condition
        toLow = "You are too low!";
        document.getElementById("inputMessage").innerHTML = toLow;
        guesses++;
        // alert('You are too low');
    } else { // Final condition
        toHigh = "You are too high!";
        document.getElementById("inputMessage").innerHTML = toHigh;
        guesses++;
        // alert('You are too high!');     
    }
}
function resetGame() {
    location.reload();
}
    
